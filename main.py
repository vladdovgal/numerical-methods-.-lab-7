import math
import matplotlib.pyplot as plt


# функція згідно варіанту
def func(x):
    return math.exp(x) * math.sin(x)


def rectangles_integral(a, b, n):
    value = 0
    x = []
    y = []
    h = (b-a)/n  # обчислюємо величину кроку
    for i in range(n):
        x.append(i*h)  # формуємо список вузлів Xi
        y.append(func(x[i]))
        value = value + h * func(x[i])  # інкрементуємо інтеграл на h*F(x[i])

    print("Кількість вузлів: ", n)
    print("Крок h: ", h)
    print("Наближене значення інтегралу: ", value, '\n')
    plt.plot(x, y, 'r.')
    plt.axis([0, math.pi,0, 8])
    plt.show()
    return value


# Вводимо значення a, b , eps
a = 0
b = math.pi
eps = 1e-4
n = 10  # початкове розбиття (довільне)
p = 2  # для формули складених прямокутників
# Обчислюємо наближене значення інтеграла Ih за формулою складених прямокутників
print("Метод складених прямокутників: ")
i_h = rectangles_integral(a, b, n)
# Повторюємо обчислення із вдвічі більшою кількістю вузлів
i_h2 = rectangles_integral(a, b, 2*n)

while abs(i_h - i_h2) > (math.pow(2,p) -1)* eps:
    n = n*2
    i_h = rectangles_integral(a, b, n)
    n = n*2
    # Знову обчислення із вдвічі більшою кількістю вузлів
    i_h2 = rectangles_integral(a, b, n)
    # Виконуємо поки не отримаємо результат із заданою точністю
print("Заданої точності eps = ", eps, " досягнуто.")